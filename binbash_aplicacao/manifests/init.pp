# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include binbash_aplicacao
class binbash_aplicacao {
exec { 'refresh_client':
  command => "/usr/bin/systemctl restart client",
  refreshonly => true,
}
exec { 'refresh_daemon_1':
  command => "/usr/bin/systemctl daemon-reload",
  refreshonly => true,
}
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/client':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/binbash_aplicacao.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/binbash_aplicacao/binbash_aplicacao.service',
        notify => Exec['refresh_daemon_1'],
     }
package { 'maven':
        ensure => installed,
        name   => $maven,
    }
remote_file { '/opt/apps/binbash_aplicacao/binbash_aplicacao.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://23.96.48.125/artfactory/binbash_aplicacao/binbash_aplicacao.jar',
    notify => Exec['refresh_client'],
  }
}
